import pandas as pd
import numpy as np

def get_title(name):
    if 'Mr.' in name:
        return 'Mr.'
    elif 'Mrs.' in name:
        return 'Mrs.'
    elif 'Miss.' in name:
        return 'Miss.'
    else:
        return 'Other'

def get_titanic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titanic_dataframe()
    df['Title'] = df['Name'].apply(get_title)
    median_ages = df.groupby('Title')['Age'].median().round().astype('int')
    missing_ages = df[df['Age'].isnull()]['Title'].value_counts()
    
    # Fill missing ages with median values
    for title in median_ages.index:
        df.loc[(df['Age'].isnull()) & (df['Title'] == title), 'Age'] = median_ages[title]
    
    results = [(title, missing_ages[title], median_ages[title]) for title in ['Mr.', 'Mrs.', 'Miss.'] if title in missing_ages]
    return results
